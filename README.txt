Choose ONE of the following tasks.
Please do not invest more than 2-4 hours on this.
Upload your results to a Github repo, for easier sharing and reviewing.

Thank you and good luck!



Code to refactor
=================
1) app/Http/Controllers/BookingController.php
2) app/Repository/BookingRepository.php

Code to write tests
=====================
3) App/Helpers/TeHelper.php method willExpireAt
4) App/Repository/UserRepository.php, method createOrUpdate


----------------------------

What I expect in your repo.

1, A readme with:   Your thoughts about the code. What makes it amazing code. Or what makes it ok code. Or what makes it terrible code. How would you have done it. Thoughts on formatting. Logic.. 

2.  Refactor it if you feel it needs refactoring. The more love you put into it. The easier for us to asses.  

Make two commits. First commit with original code. Second with your refactor so we can easily trace changes. 

NB: you do not need to set up the code on local and make the web app run. It will not run as its not a complete web app. This is purely to assess you thoughts about code, formatting, logic etc


So expected output is a GitHub link with either

1. Readme described above + refactored code 
OR
2. Readme described above + a unit test of the code that we have sent

Thank you!

Naeem Comments
==============
I chose the test for code refactoring

Code to refactor
=================
1) app/Http/Controllers/BookingController.php
2) app/Repository/BookingRepository.php

Points that are Expected in this solution
======================================

A readme with:   Your thoughts about the code. What makes it amazing code. Or what makes it ok code. Or what makes it terrible code. How would you have done it. Thoughts on formatting. Logic..

== My Thoughts about Code :
    -- The code was OK but highly coupled, low cohesive and poorly written when it comes to following the basic required Object Oriented Design Principles(SOLID) to code for an Enterprise n Scalable Application.
       Some business logic were also written in the BookingController class that wasn't the core purpose of the Controller and that should be segregated from there and should be written in a more flexible place
       where future changes or business logic can be updated without disturbing the Controller actions.

       The code structure was initially good as someone tried to follow some of the SOLID principles (incomplete)
       but when there came numerous business flows then it created a mess and all the business logic was written in a single concrete Repository Class that was totally bad (as per SOLID).
       If we follow the previously written code base and extending our application to have new features or change in features then the code will be becoming harder to maintain or change with the passage of time.
       and coupling will be becoming higher n higher that's not an ideal/wise technical approach in at least Enterprise application.

       All Controller actions(API calls) are also written inside a single Controller class that's also not a good approach.

       Code formatting and Naming conventions were good that make the code easy to read at least regarding what's going on behind the scene.

=== How would you have done it. Thoughts on formatting. Logic..

    -- I tried to follow SOLID principles as much as possible with initial understanding and segregated business logic and other functionalities in concerned repositories/services.
       I mainly tried to refactor the Repository/Business Logic Part mainly and i could segregated the Controller API actions to different dedicated controllers but i didn't do that yet for now.
       By following my approach you can easily assume that it wasn't a big deal but i tried to give you an idea about how can we refactor by playing with repository part.

------
2.  Refactor it if you feel it needs refactoring. The more love you put into it. The easier for us to asses.

I have refactored the code(Repository Part mainly) at abstract level possible as per very minimal code understanding.




