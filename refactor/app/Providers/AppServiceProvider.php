<?php namespace DTApi\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		if ($this->app->environment() == 'local') {
			$this->app->register(\Laracasts\Generators\GeneratorsServiceProvider::class);
		}

        $this->registerBindings();
	}

    /**
     * Register service provider bindings
     */
    public function registerBindings() {

        $this->app->bind(
            \DTApi\Repository\iBookingRepository::class,
            \DTApi\Repository\BookingRepository::class
        );

        $this->app->bind(
            \DTApi\Repository\iUserRepository::class,
            \DTApi\Repository\UserRepository::class
        );

        $this->app->bind(
            \DTApi\Repository\iNotificationRepository::class,
            \DTApi\Repository\NotificationRepository::class
        );

        $this->app->bind(
            \DTApi\Repository\iJobStatusRepository::class,
            \DTApi\Repository\JobStatusRepository::class
        );

        $this->app->bind(
            \DTApi\Repository\iJobHandlerRepository::class,
            \DTApi\Repository\JobHandlerRepository::class
        );

        $this->app->bind(
            \DTApi\Repository\iTranslatorRepository::class,
            \DTApi\Repository\TranslatorRepository::class
        );

        $this->app->bind(
            \DTApi\Services\iUtilityService::class,
            \DTApi\Services\UtilityService::class
        );
    }
}
