<?php

namespace DTApi\Services;

use DTApi\Exceptions\ValidationException;
use DTApi\Exceptions\Exception;

interface iUtilityService
{
    /**
     * making user_tags string from users array for creating onesignal notifications
     * @param $users
     * @return string
     */
    //private function getUserTagsStringFromArray($users);
    public function getUserTagsStringFromArray($users);

    /**
     * Convert number of minutes to hour and minute variant
     * @param int $time
     * @param string $format
     * @return string
     */
    //private function convertToHoursMins($time, $format = '%02dh %02dmin');
    public function convertToHoursMins($time, $format = '%02dh %02dmin');
}
