<?php

namespace DTApi\Repository;

use Validator;
use Illuminate\Database\Eloquent\Model;
use DTApi\Models\Job;
use DTApi\Models\User;
use Illuminate\Http\Request;
use DTApi\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface iUserRepository
{
    public function userLoginFailed();
}
