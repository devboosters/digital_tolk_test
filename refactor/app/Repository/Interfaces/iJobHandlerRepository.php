<?php

namespace DTApi\Repository;

use Validator;
use Illuminate\Database\Eloquent\Model;
use DTApi\Models\Job;
use DTApi\Models\User;
use Illuminate\Http\Request;
use DTApi\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface iJobHandlerRepository
{
    public function getUsersJobs($user_id);

    /**
     * @param $user_id
     * @return array
     */
    public function getUsersJobsHistory($user_id, Request $request);

    /**
     * @param $data
     * @return mixed
     */
    public function storeJobEmail($data);

    /**
     * @param $request
     * @return mixed
     */
    public function distance_feed(Request $request);

    /**
     * @param $job
     * @return array
     */
    public function jobToData($job);

    /**
     * @param array $post_data
     */
    public function jobEnd($post_data = array());

    /**
     * Function to get all Potential jobs of user with his ID
     * @param $user_id
     * @return array
     */
    public function getPotentialJobIdsWithUserId($user_id);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateJob($id, $data, $cuser);

    /**
     * @param $data
     * @param $user
     */
    public function acceptJob($data, $user);

    /*Function to accept the job with the job id*/
    public function acceptJobWithId($job_id, $cuser);

    public function cancelJobAjax($data, $user);

    /*Function to get the potential jobs for paid,rws,unpaid translators*/
    public function getPotentialJobs($cuser);

    public function endJob($post_data);

    public function getAll(Request $request, $limit = null);

    public function alerts();
}
