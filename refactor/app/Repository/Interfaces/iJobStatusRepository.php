<?php

namespace DTApi\Repository;

use Validator;
use Illuminate\Database\Eloquent\Model;
use DTApi\Models\Job;
use DTApi\Models\User;
use Illuminate\Http\Request;
use DTApi\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface iJobStatusRepository
{
    /**
     * @param $job
     * @param $data
     * @param $changedTranslator
     * @return array
     */
    //private function changeStatus($job, $data, $changedTranslator);
    public function changeStatus($job, $data, $changedTranslator);

    /**
     * @param $job
     * @param $data
     * @param $changedTranslator
     * @return bool
     */
    //private function changeTimedoutStatus($job, $data, $changedTranslator);
    public function changeTimedoutStatus($job, $data, $changedTranslator);

    /**
     * @param $job
     * @param $data
     * @return bool
     */
    //private function changeCompletedStatus($job, $data);
    public function changeCompletedStatus($job, $data);

    /**
     * @param $job
     * @param $data
     * @return bool
     */
    //private function changeStartedStatus($job, $data);
    public function changeStartedStatus($job, $data);

    /**
     * @param $job
     * @param $data
     * @param $changedTranslator
     * @return bool
     */
    //private function changePendingStatus($job, $data, $changedTranslator);
    public function changePendingStatus($job, $data, $changedTranslator);

    /**
     * @param $job
     * @param $data
     * @return bool
     */
    //private function changeWithdrawafter24Status($job, $data);
    public function changeWithdrawafter24Status($job, $data);

    /**
     * @param $job
     * @param $data
     * @return bool
     */
    //private function changeAssignedStatus($job, $data);
    public function changeAssignedStatus($job, $data);
}
