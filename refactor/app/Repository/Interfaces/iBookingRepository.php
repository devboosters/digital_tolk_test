<?php

namespace DTApi\Repository;

use Validator;
use Illuminate\Database\Eloquent\Model;
use DTApi\Models\Job;
use DTApi\Models\User;
use Illuminate\Http\Request;
use DTApi\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface iBookingRepository
{
    /**
     * @param $user
     * @param $data
     * @return mixed
     */
    public function store($user, $data);

    /**
     * @param $old_due
     * @param $new_due
     * @return array
     */
    //private function changeDue($old_due, $new_due);
    public function changeDue($old_due, $new_due);

    public function customerNotCall($post_data);

    public function bookingExpireNoAccepted();

    public function ignoreExpiring($id);

    public function ignoreExpired($id);

    public function ignoreThrottle($id);

    public function reopen($request);
}
