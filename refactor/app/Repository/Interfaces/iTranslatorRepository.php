<?php

namespace DTApi\Repository;

use Validator;
use Illuminate\Database\Eloquent\Model;
use DTApi\Models\Job;
use DTApi\Models\User;
use Illuminate\Http\Request;
use DTApi\Exceptions\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface iTranslatorRepository
{
    /**
     * @param Job $job
     * @return mixed
     */
    public function getPotentialTranslators(Job $job);

    /**
     * @param $current_translator
     * @param $data
     * @param $job
     * @return array
     */
    //private function changeTranslator($current_translator, $data, $job);
    public function changeTranslator($current_translator, $data, $job);
}
